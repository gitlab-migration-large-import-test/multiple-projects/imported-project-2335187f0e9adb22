# frozen_string_literal: true

module Update
  module Routers
    class NotifyRelease < ApplicationService
      using Rainbow

      include ServiceHelpersConcern

      def initialize(dependency_name, package_ecosystem)
        @dependency_name = dependency_name
        @package_ecosystem = package_ecosystem
      end

      def call
        run_within_context({ job: "notify-release", ecosystem: package_ecosystem, dependency: dependency_name }) do
          log(:info, "Triggering updates for #{package_ecosystem.bright}, package #{dependency_name.bright}")
          container_runner_class.call(
            package_ecosystem: package_ecosystem,
            task_name: "notify_release",
            task_args: [dependency_name, package_ecosystem]
          )
        end
      end

      private

      attr_reader :dependency_name, :package_ecosystem
    end
  end
end
