# Application vars
variable "gitlab_access_token" {
  description = "Gitlab access token"
  type        = string
}

variable "image_registry" {
  description = "Application docker image registry"
  type        = string
}

variable "github_access_token" {
  default     = ""
  description = "Github access token"
  type        = string
}

variable "gitlab_hooks_auth_token" {
  default     = ""
  description = "Gitlab webhook token"
  type        = string
}

variable "secret_key_base" {
  default     = ""
  description = "Application base key"
  type        = string
}

variable "redis_password" {
  default     = ""
  description = "Redis password"
  type        = string
}

variable "sentry_dsn" {
  default     = ""
  description = "Sentry DSN"
  type        = string
}

variable "dependabot_host" {
  default     = ""
  description = "Application hostname"
  type        = string
}

variable "mongodb_host" {
  default     = "dependabot.ukhoq.mongodb.net"
  description = "Mongodb hostname"
  type        = string
}

variable "mongodb_username" {
  default     = "dependabot"
  description = "Monodb username"
  type        = string
}

variable "mongodb_password" {
  default     = ""
  description = "Mongodb password"
  type        = string
}

variable "mongodb_db_name" {
  default     = "dependabot"
  description = "Mongodb database name"
  type        = string
}

variable "redis_url" {
  default     = ""
  description = "Redis url"
  type        = string
}

variable "image_tag" {
  default     = "latest"
  description = "Application docker image tag"
  type        = string
}

variable "log_level" {
  default     = "info"
  description = "Application log level"
  type        = string
}
