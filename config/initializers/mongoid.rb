# frozen_string_literal: true

default_config = lambda do |environment|
  {
    clients: {
      default: {
        hosts: [ENV["MONGODB_URL"] || "localhost:27017"],
        database: ENV["MONGODB_DATABASE"] || "dependabot_gitlab_#{environment}",
        options: {
          server_selection_timeout: 1,
          connect_timeout: 1,
          user: ENV["MONGODB_USER"],
          password: ENV["MONGODB_PASSWORD"],
          retry_writes: ENV["MONGODB_RETRY_WRITES"] || "true"
        }
      }
    }
  }
end

configuration = {
  "development" => default_config.call("development"),
  "test" => default_config.call("test"),
  "production" => if ENV["MONGODB_URI"]
                    { clients: { default: { uri: ENV["MONGODB_URI"] } } }
                  else
                    default_config.call("production").tap do |config|
                      config.dig(:clients, :default)[:options].tap do |options|
                        options[:server_selection_timeout] = 5
                        options[:connect_timeout] = 5
                      end
                    end
                  end
}

Mongoid.configure do |config|
  config.app_name = "DependabotGitlab"
  config.load_configuration(configuration[ENV["RAILS_ENV"] || "development"])
end

class DataMigration
  include Mongoid::Document

  field :version
  field :background_migration, type: Boolean, default: false
end

module Mongoid
  # :reek:InstanceVariableAssumption
  class Migrator
    class << self
      def get_all_versions(background_migration: false)
        criteria = DataMigration.where(background_migration: background_migration)
        (background_migration ? criteria : criteria.or(background_migration: nil)).map do |datamigration|
          datamigration.version.to_i
        end.sort
      end

      def rollback_to(migrations_path, target_version, background_migration: false)
        all_versions = get_all_versions(background_migration: background_migration)
        rollback_to = all_versions.index(target_version.to_i) + 1
        rollback_steps = all_versions.size - rollback_to
        rollback migrations_path, rollback_steps
      end

      def current_version(background_migration: false)
        get_all_versions(background_migration: background_migration).max || 0
      end
    end

    prepend(Module.new do
      def initialize(direction, migrations_path, target_version = nil, background_migration: false)
        super(direction, migrations_path, target_version)

        @background_migration = background_migration
      end

      def migrated
        @migrated ||= self.class.get_all_versions(background_migration: @background_migration)
      end

      def record_version_state_after_migrating(version)
        @migrated_versions ||= []

        if down?
          @migrated_versions.delete(version)
          DataMigration.where(:version => version.to_s, background_migration: @background_migration).first.destroy
        else
          @migrated_versions.push(version).sort!
          DataMigration.create(:version => version.to_s, background_migration: @background_migration)
        end
      end
    end)
  end
end
