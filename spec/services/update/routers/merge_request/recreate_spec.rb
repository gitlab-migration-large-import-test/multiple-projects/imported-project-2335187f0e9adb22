# frozen_string_literal: true

describe Update::Routers::MergeRequest::Recreate, :integration do
  subject(:recreate) do
    described_class.call(
      project_name: project.name,
      mr_iid: mr.iid,
      discussion_id: discussion_id
    )
  end

  include_context "with dependabot helper"

  let(:container_runner) { Container::Compose::Runner }
  let(:discussion_replier) { Gitlab::MergeRequest::DiscussionReplier }

  let(:project) { create(:project_with_mr) }
  let(:mr) { project.merge_requests.first }
  let(:discussion_id) { 123 }

  before do
    allow(Gitlab::MergeRequest::DiscussionReplier).to receive(:call)
    allow(container_runner).to receive(:call)
  end

  around do |example|
    with_env("SETTINGS__DEPLOY_MODE" => "compose") { example.run }
  end

  def expect_comment_reply(message)
    expect(discussion_replier).to have_received(:call).with(
      project_name: project.name,
      mr_iid: mr.iid,
      discussion_id: discussion_id,
      note: message
    )
  end

  context "without errors" do
    it "recreates merge request and replies mr status" do
      recreate

      expect_comment_reply(<<~MSG.strip)
        :warning: `dependabot-gitlab` is recreating merge request. All changes will be overwritten! :warning:
      MSG
      expect(container_runner).to have_received(:call).with(
        package_ecosystem: mr.package_ecosystem,
        task_name: "recreate_mr",
        task_args: [project.name, mr.iid, discussion_id]
      )
    end
  end

  context "with errors" do
    before do
      allow(discussion_replier).to receive(:call).and_raise("error")
    end

    it "replies recreate failure" do
      expect { recreate }.to raise_error("error")
      expect_comment_reply(":x: `dependabot-gitlab` failed to recreate merge request. :x:\n\n```\nerror\n```")
    end
  end
end
