# frozen_string_literal: true

module V1
  # @deprecated
  #
  # This API version is deprecated and will be removed in the next major release
  class NotifyRelease < Grape::API
    include AuthenticationSetup

    desc "Trigger specific dependency update for given package ecosystem across all projects"
    params do
      requires :name, type: String, desc: "Dependency name"
      requires :package_ecosystem, type: String, desc: "Package ecosystem"
    end
    post :notify_release do
      NotifyReleaseJob.perform_later(params[:name], params[:package_ecosystem])
      { triggered: true }
    end
  end
end
